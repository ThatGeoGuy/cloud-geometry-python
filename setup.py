#!/usr/bin/env python3

from setuptools import setup

setup(
    name="Cloud Geometry",
    version="0.1.7",
    install_requires=['numpy>=1.11.0', 'scipy>=0.17.0', 'attrs>=16.3.0', 'scikit-learn>=0.18.1'],
    description="Collection of tools for working with 3D objects in image metrology.",
    license='LGPLv3',
    author="Jeremy Steward",
    author_email="jeremy@thatgeoguy.ca",
    zip_safe=True,
    packages=["cloud_geometry", "cloud_geometry/keypoints"])
