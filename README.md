# Cloud Geometry

`cloud_geometry` is a small Python package containing a set of primitives and
objects for working with simple 3D data (typically point clouds). Some aspects
of the library are not written with the express intent of "*best performance*",
but rather to make the operations underlying some common point transformations
done in image metrology obvious and easy-to-use.

## Installation

Installation is easy. This package is (as of this version) private, and only
made available to a few people, however it is LGPLv3 (see License below), so in
the future I expect it will not stay private.

To install, you merely need to run the following command in the project
directory as an admin / root:

```
# python setup.py install
```

This should install the package so that you can use it from the Python
interpreter. Presently, only Python 3.4+ is supported.

## Dependencies

This library depends on the following Python libraries:

* numpy [>=1.11.0]
* scipy (specifically scipy.spatial) [>=0.17.0]
* attrs [>=16.3.0]

The first two packages can be found at [numpy.org](numpy.org)
and [scipy.org](scipy.org) respectively. `attrs` can be found at it's read the
docs [page](https://attrs.readthedocs.io/en/stable/). How you install them is up
to you. I recommend `pip` personally, however more optimized versions of `numpy`
and `scipy` may be available for your platform through different avenues.

## License

All code is copyright Jeremy Steward, 2017. The code is released under the
LGPLv3+ license. If this is unsuitable, contact Jeremy Steward
at [jeremy@thatgeoguy.ca](mailto:jeremy@thatgeoguy.ca), and further options can
be discussed depending on the need.
