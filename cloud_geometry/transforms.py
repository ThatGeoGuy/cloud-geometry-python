#!/usr/bin/env python3
# Class implementing a rigid body transformation.

import attr

from .points import Point3D
from .rotations import UnitQuaternion, EulerAngles, RotationMatrix


@attr.s(slots=True, frozen=True)
class SimilarityTransform(object):
    """
    A class that implements a 7-parameter rigid body transform for 3D points.

    Attributes:
        Translation - A 3D point that specifies how far the object is translated
                      in space.
        Rotation    - A UnitQuaternion specifying the rotation of the
                      transformation.
        Scale       - A decimal number describing the scale factor.

    Note that the class is immutable / frozen, so one cannot modify the
    translation, rotation, and scale after it is created. Create a new instance
    if this is somehow necessary.
    """
    translation = attr.ib(default=Point3D())
    rotation = attr.ib(default=UnitQuaternion(1, 0, 0, 0))
    scale = attr.ib(default=1)

    @property
    def euler_rotation(self):
        return self.rotation.to_euler_angles()

    def transform(self, point):
        """
        Transforms a single point using the 7-parameter transformation.

        @params point - The Point3D object to be transformed.

        @returns the transformed Point3D object.
        """
        return point.scale(self.scale).rotate(self.rotation).translate(
            self.translation)

    def transform_all(self, points):
        """
        Transforms a set of N points using the 7-parameter transformation.

        @params points - An N x 3 matrix containing the X, Y, Z values of all
                         coordinates to be transformed.

        @returns an N x 3 matrix containing the X, Y, Z values of all
                 transformed coordinates.
        """
        tx = self.translation.to_array()

        M = RotationMatrix.from_unit_quaternion(*self.rotation)
        return (self.scale * M.dot(points.T).T) + tx


class RigidBodyTransform(SimilarityTransform):
    """
    A class that implements a 6-parameter rigid body transform for 3D points.

    Attributes:
        Translation - A 3D point that specifies how far the object is translated
                      in space.
        Rotation    - A UnitQuaternion specifying the rotation of the
                      transformation.

    Like the SimilarityTransform class, this is an immutable / frozen class. A
    new instance should be created if one wishes to update the translation /
    rotation values after creation.
    """

    def __init__(self, translation, rotation):
        """
        Initializes the class given a set of 3 translations, 3 rotations, and a
        scale parameter.
        """
        super().__init__(translation, rotation, 1.0)

    def __str__(self):
        return "RigidBodyTransform({}, {})".format(self.translation,
                                                   self.rotation)
