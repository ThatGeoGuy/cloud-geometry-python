#!/usr/bin/env python3
## Basic point operations
# Don't expect much of anything fancy

from collections import namedtuple
from math import sqrt

import attr
import numpy as np

from .quaternion import Quaternion
from .rotations import UnitQuaternion, EulerAngles


@attr.s(slots=True)
class Point2D(object):
    """
    Simple two-dimensional point class.
    """
    X = attr.ib(default=0)
    Y = attr.ib(default=0)

    def __neg__(self):
        """
        Negates the point. Multiplies all components of the point by negative
        one.
        """
        return Point2D(-self.X, -self.Y)

    def scale(self, factor):
        """
        Scales the point by the given scale factor.

        @param factor - The scale factor to scale the point by (relative to the
        origin).

        @returns a new point scaled by the scale factor.
        """
        return Point2D(factor * self.X, factor * self.Y)

    def rotate(self, unit_quaternion):
        """
        Rotates the point into a new coordinate frame via quaternion
        multiplication.

        @param unit_quaternion - The unit quaternion describing the rotation.

        @returns a new point rotated to the new coordinate frame.
        """
        q = UnitQuaternion(*unit_quaternion)
        p = Quaternion(0, self.X, self.Y, 0)
        pr = q.conjugate() * p * q

        return Point2D(pr.xi, pr.yj)

    def translate(self, txy):
        """
        Translates the point by adding each individual component of txy
        to the point.

        @param txy - The point storing the translation components in X, and Y
                     (i.e. effectively the new origin in the old system.)

        @returns a new point translated to the new origin.
        """
        return Point2D(self.X + txyz.X, self.Y + txyz.Y)

    def to_array(self):
        """
        Transforms the point into a 1 x 2 numpy array, of the form [X, Y]

        @returns a numpy array of the form [X, Y]
        """
        return np.array([self.X, self.Y])


@attr.s(slots=True)
class Point3D(object):
    """
    Simple three-dimensional point class.
    """
    X = attr.ib(default=0)
    Y = attr.ib(default=0)
    Z = attr.ib(default=0)

    def __neg__(self):
        """
        Negates the point. Multiplies all components of the point
        by negative one.
        """
        return Point3D(-self.X, -self.Y, -self.Z)

    def scale(self, factor):
        """
        Scales the point by the given scale factor.

        @param factor - The scale factor to scale the point by (relative to the
        origin).

        @returns a new point scaled by the scale factor.
        """
        return Point3D(factor * self.X, factor * self.Y, factor * self.Z)

    def rotate(self, unit_quaternion):
        """
        Rotates the point into a new coordinate frame via quaternion
        multiplication.

        @param unit_quaternion - The unit quaternion describing the rotation.

        @returns a new point rotated to the new coordinate frame.
        """
        q = UnitQuaternion(*unit_quaternion)
        p = Quaternion(0, self.X, self.Y, self.Z)
        pr = q.conjugate() * p * q

        return Point3D(pr.xi, pr.yj, pr.zk)

    def translate(self, txyz):
        """
        Translates the point by adding each individual component of txyz
        to the point.

        @param txyz - The point storing the translation components in X, Y, and Z
                      (i.e. effectively the new origin in the old system.)

        @returns a new point translated to the new origin.
        """
        return Point3D(self.X + txyz.X, self.Y + txyz.Y, self.Z + txyz.Z)

    def to_quaternion(self):
        """
        Converts a point to a quaternion. Useful for rotations using
        Quaternions.

        @returns a purely imaginary quaternion with values w=0, xi=x, yj=y, zk=z
        """
        return Quaternion(0, self.X, self.Y, self.Z)

    def to_array(self):
        """
        Transforms the point into a 1 x 3 numpy array, of the form [X, Y, Z]

        @returns a numpy array of the form [X, Y, Z]
        """
        return np.array([self.X, self.Y, self.Z])


def distance(from_pt, to_pt):
    """
    Computes the distance between two points.

    @param from_pt - the point to calculate the distance from.
    @param to_pt - the point to calculate the distance to.

    @returns the scalar distance between two points.
    """
    if (len(attr.astuple(from_pt)) != len(attr.astuple(to_pt))):
        raise ValueError(
            "Both points must have the same number of dimensions.", from_pt,
            to_pt)

    return sqrt(sum((f - t)**2
                    for f, t in zip(
                        attr.astuple(from_pt), attr.astuple(to_pt))))


def centroid(points):
    """
    Computes the centroid (average X, Y, and Z) of a set of points.

    @params points - A matrix of n columns and 3 rows representing X, Y, Z.
    @returns A numpy array (1D) with columns for X, Y, and Z
    """
    arr = np.array([attr.astuple(p) for p in points])
    return Point3D(*np.mean(arr, axis=0))
