#!/usr/bin/env python3
## units.py
# Implements some common unit conversions, as well
# as namedtuples for custom units (e.g. EulerAngles, DMS)

from collections import namedtuple
from math import pi

import attr

@attr.s(slots=True, frozen=True)
class DMS(object):
    """
    Holder class for degrees-minutes-seconds representation of angles.
    """
    degrees = attr.ib(default=0)
    minutes = attr.ib(default=0)
    seconds = attr.ib(default=0)


def metres_to_inches(length_in_metres):
    """
    Converts a length from units of metres to units of inches.

    @params length_in_metres - the length to convert, in metres.
    @returns The length in inches.
    """
    return length_in_metres * 39.370


def inches_to_metres(length_in_inches):
    """
    Converts a length from units of inches to units of metres.

    @params length_in_inches - the length to convert, in inches.
    @returns The length in metres.
    """
    return length_in_inches / 39.370


def normalize_angle(angle):
    """
    Normalizes an angle in degrees between [-180, 180]
    """
    ret = angle
    while ret > 180:
        ret -= 360

    while ret < -180:
        ret += 360

    return ret


def radians_to_degrees(angle):
    """
    Converts an angle from radians to decimal degrees.

    @params angle - the angle to convert, in decimal degrees.
    @returns The provided angle in radians.
    """
    return normalize_angle(angle * 180 / pi)


def degrees_to_radians(angle):
    """
    Converts an angle from decimal degrees to radians.

    @params angle - the angle to convert, in radians.
    @returns The provided angle in decimal degrees.
    """
    return angle * pi / 180


def dms_to_degrees(dms_angle):
    """
    Converts an angle from degrees-minutes-seconds to an angle in decimal
    degrees.

    @params dms_angle - angle with .degrees, .minutes, .seconds members.

    @returns The angle in decimal degrees.
    """
    return normalize_angle(dms_angle.degrees + (dms_angle.minutes / 60) + (
        dms_angle.seconds / 3600))


def degrees_to_dms(angle):
    """
    Converts an angle from decimal degrees to an angle in degrees-minutes
    -seconds.

    @params angle - an angle in decimal degrees.

    @returns an angle in degrees-minutes-seconds treated as a namedtuple type.
    """
    degrees = int(angle)
    minutes = int((angle - degrees) * 60)
    seconds = (((angle - degrees) * 60) - minutes) * 60

    return DMS(degrees, minutes, seconds)


def dms_to_radians(dms_angle):
    """
    Converts an angle from degrees-minutes-seconds to an angle in radians.

    @params dms_angle - angle with .degrees, .minutes, .seconds members.

    @returns the provided angle in radians
    """
    return degrees_to_radians(dms_to_degrees(dms_angle))


def radians_to_dms(angle):
    """
    Converts an angle from radians to an angle in degrees-minutes-seconds.

    @params angle - the provided angle in radians

    @returns an angle in degrees-minutes-seconds treated as a namedtuple type.
    """
    return degrees_to_dms(radians_to_degrees(angle))
