#!/usr/bin/env python3
# Defines the point detector used by the Intrinsic Shape Signatures algorithm.

import numpy as np
from sklearn.neighbors import KDTree


class IntrinsicShapeSignatures(object):
    """
    A class to manage the state and functionality used in the Intrinsic Shape
    Signature algorithm for detecting keypoints.

    This class is constructed with the following arguments:

    @params points         - the 3D point data to perform keypoint detection on.
    @params density_radius - the radius of neighbourhood of points to compute
                            weights.
    @params frame_radius   - the radius of the intrinsic reference frame.
    @params gamma21        - the first saliency threshold.
    @params gamma32        - the second saliency threshold.
    """

    def __init__(self, points, density_radius, frame_radius, gamma21, gamma32):
        self._kdtree = KDTree(points)
        self._density_radius = density_radius
        self._frame_radius = frame_radius
        self._gamma21 = gamma21
        self._gamma32 = gamma32

    @property
    def data(self):
        """
        Returns the points stored within our KDTree.
        """
        return np.array(self._kdtree.data)

    @data.setter
    def data(self, new_points):
        """
        Sets the points stored within our KDTree to new_points
        """
        self._kdtree = KDTree(new_points)
        return

    def _weighted_scatter_matrices(self):
        """
        Computes the weighted scatter matrix cov(pt_i) for pt_i using all points pt_j
        within a distance frame_radius.

        @params w - a vector of weights for each point. w[i] corresponds to pt_i.

        @returns a list of scatter matrices for each point i.
        """
        cvs = []
        sum_weight = 0

        for i in range(len(self.data)):
            pt_i = self.data[i, :].reshape(1, -1)
            nn_indices = self._kdtree.query_radius(
                pt_i,
                self._frame_radius)

            nn_indices = nn_indices[0]
            count = len(nn_indices)

            # Remove the point itself from it's nearest neighbours
            nn_indices = nn_indices[nn_indices != i]

            # Need at least 3 points to determine proper eigenvalues
            # from this covariance matrix.
            if count < 3:
                cvs.append(np.zeros((3, 3)))
                continue

            weight = 1 / count
            cvs.append(weight * np.cov((self.data[nn_indices, :] - pt_i).T))
            sum_weight += weight
        return (cv / sum_weight for cv in cvs)

    def keypoints(self):
        """
        Detects and returns the points determined to be keypoints via the ISS
        saliency measure with non-maxima suppression.

        @returns the keypoints detected by the ISS algorithm
        """
        covars = self._weighted_scatter_matrices()
        saliencies = np.zeros(len(self.data))

        for i, cv in enumerate(covars):
            try:
                evals, evecs = np.linalg.eigh(cv)
            except np.linalg.LinAlgError:
                continue

            if not np.all(evals == 0):
                lam1 = evals[2]
                lam2 = evals[1]
                lam3 = evals[0]

                if lam3 < 0:
                    continue

                if ((lam2 / lam1) < self._gamma21) and (
                        (lam3 / lam2) < self._gamma32):
                    saliencies[i] = lam3

        self._keypoint_indices = []
        for i in range(len(self.data)):
            pt_i = self.data[i, :].reshape(1, -1)
            nn_indices = self._kdtree.query_radius(
                    pt_i,
                    self._frame_radius)

            nn_indices = nn_indices[0]
            # Remove the point itself from it's nearest neighbours
            nn_indices = nn_indices[nn_indices != i]

            if nn_indices.shape[0] and (
                    saliencies[i] >= np.max(saliencies[nn_indices])):
                self._keypoint_indices.append(i)

        return self.data[self._keypoint_indices, :].copy()

