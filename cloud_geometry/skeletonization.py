#!/usr/bin/env python3
# Performs the skeletonization on a point cloud

import numpy as np
from scipy.spatial import Delaunay, Voronoi

from .boundary import boundary


def skeletonize(points):
    """
    Finds the interior approximate medial axis of a point cloud, using the
    method laid out by Yoshizawa et al. (2007).

    @params points - an N x K matrix representing N points in K dimensions

    @returns - an M x K matrix of points along the medial axis of the bounded
               shape described by the input points.
    """
    outer_points = np.array(list(boundary(points, 2)))

    vor = Voronoi(outer_points)
    tri = Delaunay(outer_points)

    # Medial Axis IDs for vertices in Voronoi diagram
    ma_id = np.ones(vor.vertices.shape[0], dtype=bool)

    for i in range(len(outer_points)):
        vs = [v for v in vor.regions[vor.point_region[i]] if v >= 0]
        for v in vs:
            if tri.find_simplex(vor.vertices[v, :]) == -1:
                ma_id[v] = False

    medial_axis_vertices = vor.vertices[ma_id]
    return medial_axis_vertices
