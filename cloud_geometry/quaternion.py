#!/usr/bin/env python3
## Quaternion.py
# Implements a class representing quaternions.
# Associated quaternion<->quaternion operations are provided.

from math import sqrt

import attr


@attr.s(slots=True, frozen=True)
class Quaternion(object):
    """
    Defines a class describing Quaternions. Quaternions can be thought
    of as a special case of complex numbers where there are 3 complex
    components along orthogonal axes. They are represented as such:

        W + X i + Y j + Z k = 0

    Where W is the real component of the quaternion, and X, Y, Z are the
    complex components in i, j, k directions respectively.

    Note that Quaternion is an immutable class, so individual components
    can only be modified if a new instance is generated. The class is
    made immutable to reduce the errors of manually fiddling with the
    quaternion components outside of well defined operations.
    """
    w = attr.ib(default=0)
    xi = attr.ib(default=0)
    yj = attr.ib(default=0)
    zk = attr.ib(default=0)

    @property
    def magnitude(self):
        """
        Property returning the vector magnitude of the quaternion.
        """
        return sqrt(self.w**2 + self.xi**2 + self.yj**2 + self.zk**2)

    @classmethod
    def _make(cls, w, xi, yj, zk):
        """
        Helper method to make an instance of the quaternion class or any
        derivative class that calls one of the methods that constructs a new
        quaternion type.
        """
        return cls(w, xi, yj, zk)

    def __neg__(self):
        """
        Negates the Quaternion. A negated quaternion has the same components
        as the original, but in the opposite direction (negative instead of
        positive and vice-versa)

        @returns A quaternion with the same magnitude but opposite signs for
                 each component.
        """
        return self._make(-self.w, -self.xi, -self.yj, -self.zk)

    def __pos__(self):
        """
        Unary positive operator. Does nothing, returns the quaternion.
        """
        return self

    def __add__(self, other):
        """
        Quaternion addition operator.
        """
        return self._make(self.w + other.w, self.xi + other.xi,
                          self.yj + other.yj, self.zk + other.zk)

    def __sub__(self, other):
        """
        Quaternion subtraction operator.
        """
        return self + (-other)

    def __mul__(self, other):
        """
        Quaternion multiplication operator.
        """
        w = (self.w * other.w) - (self.xi * other.xi) - (
            self.yj * other.yj) - (self.zk * other.zk)
        x = (self.w * other.xi) + (self.xi * other.w) + (
            self.yj * other.zk) - (self.zk * other.yj)
        y = (self.w * other.yj) - (self.xi * other.zk) + (
            self.yj * other.w) + (self.zk * other.xi)
        z = (self.w * other.zk) + (self.xi * other.yj) - (
            self.yj * other.xi) + (self.zk * other.w)

        return self._make(w, x, y, z)

    def __truediv__(self, other):
        """
        Quaternion division operator.
        """
        r = other.magnitude**2
        w = (self.w * other.w + self.xi * other.xi + self.yj * other.yj +
             self.zk * other.zk) / r
        x = (self.xi * other.w - self.w * other.xi - self.zk * other.yj +
             self.yj * other.zk) / r
        y = (self.yj * other.w + self.zk * other.xi - self.w * other.yj -
             self.xi * other.zk) / r
        z = (self.zk * other.w - self.yj * other.xi + self.xi * other.yj -
             self.w * other.zk) / r

        return self._make(w, x, y, z)

    def conjugate(self):
        """
        Produces the conjugate quaternion of the quaternion this is called on.
        The conjugate is similar to the inverse / negation, however only the
        non-real (i.e. imaginary or complex) components are negated, while the
        real component is left as is.
        """
        return self._make(self.w, -self.xi, -self.yj, -self.zk)

    def inverse(self):
        """
        Produces the inverse of the input quaternion. The inverse is the conjugate
        with each component divided by the square magnitude.
        """
        square_norm = self.magnitude**2
        w = self.w / square_norm
        xi = -self.xi / square_norm
        yj = -self.yj / square_norm
        zk = -self.zk / square_norm
        return self._make(w, xi, yj, zk)

    def __invert__(self):
        """
        Syntactical operator for the inverse of a quaternion.
        """
        return self.Inverse()

    def dot(self, other):
        """
        Calculates the dot product of two quaternions.

        @returns the numerical magnitude of the dot product.
        """
        return (self.w * other.w) + (self.xi * other.xi) + (
            self.yj * other.yj) + (self.zk * other.zk)

    def cross(self, other):
        """
        Calculates the cross product of two quaternions.
        NOTE: The result is always just a quaternion since there's
        no guarantee that there exists a quaternion of unit length orthogonal
        to both input quaternions.

        @returns a quaternion representing the cross product of two quaternions.
        """
        w = -(self.dot(other))
        xi = (self.yj * other.zk) - (self.zk * other.yj)
        yj = (self.zk * other.xi) - (self.xi * other.zk)
        zk = (self.xi * other.yj) - (self.yj * other.xi)
        return Quaternion(w, xi, yj, zk)

    def __iter__(self):
        """
        Magic method for iterating over a quaternion as if it was a list.
        Helpful for passing in quaternion components instead of the full
        class for some interfaces.

        e.g.
            p = Quaternion(1, 2, 3, 4)
            q = Quaternion(5, 6, 7, 8)
            quaternions = np.array([tuple(p), tuple(q)])
        """
        for item in attr.astuple(self):
            yield item
