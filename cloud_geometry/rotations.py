#!/usr/bin/env python3
## rotations.py
# Implements useful data types to work with rotations.

from math import sin, cos, atan2, asin, sqrt

import attr
import numpy as np

from .units import degrees_to_radians, radians_to_degrees
from .quaternion import Quaternion


class UnitQuaternion(Quaternion):
    """
    Defines a class describing Unit Quaternions. A unit quaternion is a
    quaternion which has a unit magnitude (1). Such quaternions can be
    used to express rotations in space.
    """

    def __init__(self, w, xi, yj, zk):
        """
        Normalizes the unit quaternion upon initialization.
        """
        super(UnitQuaternion, self).__init__(*self._normalize(w, xi, yj, zk))

    @staticmethod
    def _normalize(w, xi, yj, zk):
        """
        Normalizes the magnitude of quaternion components to unit length.

        @params w - The real component of the quaternion
        @params xi - The first complex component of the quaternion
        @params yj - The second complex component of the quaternion
        @params zk - The third complex component of the quaternion

        @returns (w / mag, xi / mag, yj / mag, zk / mag) where mag is the
                 vector magnitude of the quaternion components.
        """
        magnitude = sqrt(w**2 + xi**2 + yj**2 + zk**2)
        return (w / magnitude, xi / magnitude, yj / magnitude, zk / magnitude)

    def to_euler_angles(self):
        """
        Converts a Unit Quaternion to a set of Euler Angles.

        @returns a set of three Euler Angles (omega,phi,kappa) that specify
                 rotations about X, Y, Z respectively.
        """
        w, x, y, z = self.w, self.xi, self.yj, self.zk

        omega = atan2(-2 * (y * z - w * x), w**2 - x**2 - y**2 + z**2)
        phi = asin(2 * (x * z + w * y))
        kappa = atan2(-2 * (x * y - w * z), w**2 + x**2 - y**2 - z**2)
        return EulerAngles(
            radians_to_degrees(omega), radians_to_degrees(phi),
            radians_to_degrees(kappa))

    @staticmethod
    def from_euler_angles(euler_angles):
        """
        Converts a set of Euler Angles to a Unit Quaternion

        @params euler_angles - the Euler angles to convert

        @returns a unit quaternion describing the same rotation as the Euler
                 angles.
        """
        return euler_angles.to_unit_quaternion()

    @staticmethod
    def from_rotation_matrix(rotation_matrix):
        """
        Converts a set of direction cosines (structured as a matrix) into a
        Unit Quaternion.

        @params rotation_matrix - the rotation matrix containing direction
                                  cosines.

        @returns a unit quaternion describing the same rotation as the provided
                 direction cosines.
        """
        return EulerAngles.from_rotation_matrix(
            rotation_matrix).to_unit_quaternion()

    def __mul__(self, other):
        """
        Multiplication operator for Unit Quaternions.

        Has to be different than the base class because whenever we multiply a
        Quaternion by a UnitQuaternion (in any order) we want a Quaternion, not
        a UnitQuaternion. However if we multiply two UnitQuaternions, we expect
        that we should get a UnitQuaternion as a result. For most scenarios,
        this would work with the standard dispatch provided by inheriting from
        the Quaternion class, however in one case we get the wrong result:

            q = UnitQuaternion(1, 0, 0, 0) * Quaternion(0, 3, 3, 3)

        We expect that q should be of class Quaternion and should be equal to
        Quaternion(0, 3, 3, 3), but because __mul__ is called on the
        UnitQuaternion, the final result ends up being a UnitQuaternion as well
        (since UnitQuaternion is passed to the classmethod _make). To fix this,
        when multiplying UnitQuaternions we should consider the class of
        'other', not 'self', since we want to use the class of the second
        operand in the equation.

        Unfortunately calling the method from the superclass doesn't work here,
        as that will still depend on the class of self.  Instead the code needs
        to be repeated, and the call to _make needs to come from 'other', not
        'self'.
        """
        w = (self.w * other.w) - (self.xi * other.xi) - (
            self.yj * other.yj) - (self.zk * other.zk)
        x = (self.w * other.xi) + (self.xi * other.w) + (
            self.yj * other.zk) - (self.zk * other.yj)
        y = (self.w * other.yj) - (self.xi * other.zk) + (
            self.yj * other.w) + (self.zk * other.xi)
        z = (self.w * other.zk) + (self.xi * other.yj) - (
            self.yj * other.xi) + (self.zk * other.w)

        return other._make(w, x, y, z)

    def __truediv__(self, other):
        """
        Division operator for Unit Quaternions.

        Same as when performing multiplication, we need to make this method
        different from the base class because we want a Quaternion as a final
        result of the division if the second operand is a Quaternion, and a
        UnitQuaternion if the second operand is a UnitQuaternion.

        Unfortunately calling the method from the superclass doesn't work here,
        as that will still depend on the class of self.  Instead the code needs
        to be repeated, and the call to _make needs to come from 'other', not
        'self'.
        """
        r = other.magnitude**2
        w = (self.w * other.w + self.xi * other.xi + self.yj * other.yj +
             self.zk * other.zk) / r
        x = (self.xi * other.w - self.w * other.xi - self.zk * other.yj +
             self.yj * other.zk) / r
        y = (self.yj * other.w + self.zk * other.xi - self.w * other.yj -
             self.xi * other.zk) / r
        z = (self.zk * other.w - self.yj * other.xi + self.xi * other.yj -
             self.w * other.zk) / r

        return other._make(w, x, y, z)

    def __str__(self):
        """
        String representation for UnitQuaternion
        """
        return "Unit" + super(UnitQuaternion, self).__str__()


@attr.s(slots=True, frozen=True)
class EulerAngles(object):
    """
    Defines a class describing Euler Angles, a set of three angles
    specifying rotations about X (omega), Y (phi), and Z (kappa) Cartesian
    axes in degrees, respectively.

    Note that the Euler Angles class is immutable, so individual components
    cannot be modified unless a new instance is generated. This is done to
    reduce the errors of manually fiddling with components outside well
    defined operations.
    """
    omega = attr.ib(default=0)
    phi = attr.ib(default=0)
    kappa = attr.ib(default=0)

    def __iter__(self):
        """
        Magic method for iterating over Euler angles as if they were a list.
        Helpful for passing in the Euler angles instead of the full
        class for some interfaces.

        e.g.
            e = EulerAngles(90, 0, 45)
            r = RotationMatrix.from_euler_angles(*e)
        """
        for item in attr.astuple(self):
            yield item

    def to_unit_quaternion(self):
        """
        Converts a set of Euler Angles to a Unit Quaternion

        @params euler_angles - the Euler angles to convert

        @returns a unit quaternion describing the same rotation as the Euler angles.
        """
        o = degrees_to_radians(self.omega) / 2
        p = degrees_to_radians(self.phi) / 2
        k = degrees_to_radians(self.kappa) / 2

        q0 = cos(o) * cos(p) * cos(k) - sin(o) * sin(p) * sin(k)
        q1 = sin(o) * cos(p) * cos(k) + cos(o) * sin(p) * sin(k)
        q2 = cos(o) * sin(p) * cos(k) - sin(o) * cos(p) * sin(k)
        q3 = cos(o) * cos(p) * sin(k) + sin(o) * sin(p) * cos(k)

        return UnitQuaternion(q0, q1, q2, q3)

    @staticmethod
    def from_unit_quaternion(q):
        """
        Converts a Unit Quaternion to a set of Euler Angles.

        @returns a set of three Euler Angles (omega, phi, kappa) that specify
                 rotations about X, Y, Z respectively.
        """
        return q.to_euler_angles()

    @staticmethod
    def from_rotation_matrix(r):
        """
        Converts a given rotation matrix (3x3) to a set of EulerAngles,
        in degrees.

        @param r - the 3x3 rotation matrix to extract the rotations from.

        @returns a set of three Euler Angles (omega, phi, kappa) that specify
                 rotatoins about X, Y, Z respectively.
        """
        o = radians_to_degrees(atan2(-r[2, 1], r[2, 2]))
        p = radians_to_degrees(asin(r[2, 0]))
        k = radians_to_degrees(atan2(-r[1, 0], r[0, 0]))
        return EulerAngles(o, p, k)


class RotationMatrix(object):
    """
    A static factory object to construct 3D rotation matrices (clockwise
    positive). Matrices can be constructed from Euler angles (omega, phi,
    kappa), Angle-Axis / Exponential Map (angle, axis), or Quaternions (q0, q1,
    q2, q3).

    To use this class, do not instantiate it (i.e. don't R = RotationMatrix()).
    Instead, opt to call the static methods and return direct numpy arrays to
    use as rotation matrices.
    """

    def __init__(self):
        raise NotImplementedError("RotationMatrix cannot be instantiated.")

    @staticmethod
    def _R1(t):
        """
        Generates 3D rotation matrix (clockwise positive) about X axis from an
        angle in radians.

        @param t - angle theta in radians.
        @returns A 3D rotation matrix about the X axis.
        """
        return np.array([[1, 0, 0], [0, cos(t), sin(t)], [0, -sin(t), cos(t)]])

    @staticmethod
    def _R2(t):
        """
        Generates 3D rotation matrix (clockwise positive) about Y axis from an
        angle in radians.

        @param t - angle theta in radians.
        @returns A 3D rotation matrix about the Y axis.
        """
        return np.array([[cos(t), 0, -sin(t)], [0, 1, 0], [sin(t), 0, cos(t)]])

    @staticmethod
    def _R3(t):
        """
        Generates 3D rotation matrix (clockwise positive) about Z axis from an
        angle in radians.

        @param t - angle theta in radians.
        @returns A 3D rotation matrix about the Z axis.
        """
        return np.array([[cos(t), sin(t), 0], [-sin(t), cos(t), 0], [0, 0, 1]])

    @staticmethod
    def from_euler_angles(omega, phi, kappa):
        """
        Generates a 3D rotation matrix (clockwise positive) from Euler angles
        in degrees.

        @param omega - Rotation about X axis
        @param phi   - Rotation about Y axis
        @param kappa - Rotation about Z axis

        @returns A 3D rotation matrix derived from 3 Euler angles.
        """
        r1 = RotationMatrix._R1(degrees_to_radians(omega))
        r2 = RotationMatrix._R2(degrees_to_radians(phi))
        r3 = RotationMatrix._R3(degrees_to_radians(kappa))

        return r3.dot(r2.dot(r1))

    @staticmethod
    def from_angle_axis(angle, axis):
        """
        Generates a 3D rotation matrix (clockwise positive) from Angle-Axis
        parameters. Angle-Axis is also known as the 'exponential map.'

        @param angle - Rotation about axis in radians.
        @param axis - A vector describing the axis of rotation in Cartesian
                      space (e.g. [0, 1, 0] defines the Y-axis).

        @returns A 3D rotation matrix derived from Angle-Axis parameters.
        """
        magnitude = np.linalg.norm(axis, 2)
        n1 = axis[0] / magnitude
        n2 = axis[1] / magnitude
        n3 = axis[2] / magnitude
        t = degrees_to_radians(angle)

        M = np.zeros((3, 3))
        M[0, 0] = cos(t) + (n1**2) * (1 - cos(t))
        M[0, 1] = (n1 * n2) * (1 - cos(t)) + (n3 * sin(t))
        M[0, 2] = (n1 * n3) * (1 - cos(t)) - (n2 * sin(t))

        M[1, 0] = (n1 * n2) * (1 - cos(t)) - (n3 * sin(t))
        M[1, 1] = cos(t) + (n2**2) * (1 - cos(t))
        M[1, 2] = (n2 * n3) * (1 - cos(t)) + (n1 * sin(t))

        M[2, 0] = (n1 * n3) * (1 - cos(t)) + (n2 * sin(t))
        M[2, 1] = (n2 * n3) * (1 - cos(t)) - (n1 * sin(t))
        M[2, 2] = cos(t) + (n3**2) * (1 - cos(t))

        return M

    @staticmethod
    def from_unit_quaternion(q0, q1, q2, q3):
        """
        Generates a 3D rotation matrix (clockwise positive) from a Quaternion.
        A quaternion is defined here as the four separate parameters q0, q1,
        q2, and q3. Ideally the quaternion vector should be normalized before
        being passed into this method, howwever this method will normalize the
        quaternion before creating the rotation matrix just in case.

        @param q0 - The first quaternion component. Represents the scalar
                    portion of the quaternion.
        @param q1 - The second quaternion component. Represents the `i` complex
                    component of the quaternion.
        @param q2 - The third quaternion component. Represents the `j` complex
                    component of the quaternion.
        @param q3 - The fourth quaternion component. Represents the `k` complex
                    component of the quaternion.

        @returns A 3D rotation matrix derived from a quaternion.
        """
        magnitude = np.linalg.norm([q0, q1, q2, q3], 2)

        if abs(magnitude) < 1e-12:
            raise ValueError("Invalid quaternion (magnitude cannot be zero).")

        q0 /= magnitude
        q1 /= magnitude
        q2 /= magnitude
        q3 /= magnitude

        M = np.zeros((3, 3))
        M[0, 0] = (q0**2) + (q1**2) - (q2**2) - (q3**2)
        M[0, 1] = 2 * (q1 * q2 + q0 * q3)
        M[0, 2] = 2 * (q1 * q3 - q0 * q2)

        M[1, 0] = 2 * (q1 * q2 - q0 * q3)
        M[1, 1] = (q0**2) - (q1**2) + (q2**2) - (q3**2)
        M[1, 2] = 2 * (q2 * q3 + q0 * q1)

        M[2, 0] = 2 * (q1 * q3 + q0 * q2)
        M[2, 1] = 2 * (q2 * q3 - q0 * q1)
        M[2, 2] = (q0**2) - (q1**2) - (q2**2) + (q3**2)

        return M
