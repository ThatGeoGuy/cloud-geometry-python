#!/usr/bin/env python3
# Finds the boundary points (in 3D) of a point cloud.

import numpy as np
from scipy.spatial import Voronoi


def boundary(points, alpha):
    """
    Finds the boundary K-dimensional points in a given set of points by using
    alpha shapes and detecting alpha-extreme points. Alpha extreme points can be
    computed as found in Fayed and Mouftah (2009).

    @params points - an N x K matrix representing points in X, Y, Z

    @params alpha - the alpha factor to determine the alpha boundaries of the
                    shape. The radius compares to 1 / alpha. If alpha is zero,
                    then the convex hull is returned, otherwise a concave hull
                    is returned for the given alpha.

    @returns - an M x K matrix of points along the boundary of the input point
               cloud.
    """
    r = np.inf if alpha == 0 else (1 / alpha)
    vor = Voronoi(points)

    # Detecting alpha-extreme points as described by Fayed and Mouftah (2008)
    for i, pt in enumerate(points):
        region = vor.regions[vor.point_region[i]]
        vertices = vor.vertices[region]

        dists = np.linalg.norm(vertices - pt, 2, axis=1)

        if np.any(r <= dists):
            yield pt
